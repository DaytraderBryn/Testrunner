package runner;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.libtrader.api.ApiWrapper;
import org.libtrader.api.exceptions.TWSException;
import org.libtrader.types.ConfigHandler;

import java.io.PrintStream;
import java.net.URL;
import java.util.Optional;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        if (!ConfigHandler.checkConfigDirExists()) {
            ConfigHandler.createConfigDir();
        }
        try {
            ApiWrapper.getInstance().connect();
        } catch (TWSException exception) {
            ButtonType offlineButtonType = new ButtonType("Offline Mode");
            ButtonType exitButtonType = new ButtonType("Exit");

            Alert errorBox = new Alert(Alert.AlertType.ERROR,
                    "Attempt to connect in offline mode? (You won't be able to access any trades but you can use data already tested on)",
                    offlineButtonType,
                    exitButtonType);
            errorBox.setTitle("An error occurred while connecting to TWS");
            errorBox.setHeaderText(exception.getLocalizedMessage());

            errorBox.showAndWait().ifPresent(buttonType -> {
                if (buttonType == offlineButtonType) {
                    ApiWrapper.getInstance().connectOffline();
                } else {
                    exit();
                }
            });
        } finally {
            URL path = getClass().getResource("/main.fxml");
            Parent root = FXMLLoader.load(path);
            primaryStage.setTitle("Daytrader Test Runner");
            primaryStage.setScene(new Scene(root, 833, 900));
            primaryStage.setOnCloseRequest(e -> exit());
            primaryStage.show();
        }
    }

    private void exit() {
        Platform.exit();
        System.exit(0);
    }


    public static void main(String[] args) {
        launch(args);
    }
}
