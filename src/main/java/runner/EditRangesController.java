package runner;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import javafx.fxml.FXML;
import javafx.scene.control.Accordion;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import jfxtras.scene.control.LocalTimeTextField;
import org.libtrader.types.ConfigHandler;
import org.libtrader.types.enums.PutupClass;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by beanemcbean on 16/06/2016.
 */
public class EditRangesController {

    @FXML
    private BorderPane borderPane;

    @FXML
    private Accordion accordion;

    @FXML
    private void initialize() {
        JsonObject rangeList = new JsonObject();
        try {
            JsonParser parser = new JsonParser();
            String filePath = ConfigHandler.getFilePath("range.json");
            JsonElement element = parser.parse(new FileReader(filePath));
            rangeList = element.getAsJsonObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        List<String> rangeKeys = rangeList.entrySet()
                .stream()
                .map(i -> i.getKey())
                .collect(Collectors.toCollection(ArrayList::new));

        for (String rangeName : rangeKeys) {
            JsonElement group = rangeList.get(rangeName);
            if (group.isJsonObject()) {
                JsonObject groupObj = group.getAsJsonObject();
                HBox hbox = new HBox();
                for (PutupClass classEnum : PutupClass.values()) {
                    hbox.getChildren().add(
                            createNewEditBox(classEnum.name(),groupObj.get(classEnum.name()).getAsString()));
                }
                TitledPane tp = new TitledPane();
                tp.setText(rangeName);
                tp.setContent(hbox);
                accordion.getPanes().add(tp);
            } else if (group.isJsonArray()) {
                JsonArray groupAry = group.getAsJsonArray();
                VBox container = new VBox();
                for (int i = 0; i< groupAry.size(); i++) {
                    JsonObject groupObj = groupAry.get(i).getAsJsonObject();
                    HBox hbox = new HBox();
                    hbox.getChildren().addAll(
                            createNewTimeBox("Start time", groupObj.get("startTime").getAsJsonObject()),
                            createNewTimeBox("End time", groupObj.get("endTime").getAsJsonObject())
                    );
                    for (PutupClass classEnum : PutupClass.values()) {
                        hbox.getChildren().addAll(
                                createNewEditBox(classEnum.name() + "min", groupObj
                                        .get("ranges").getAsJsonObject()
                                        .get(classEnum.name()).getAsJsonObject()
                                        .get("min").getAsString()),
                                createNewEditBox(classEnum.name() + "max", groupObj
                                        .get("ranges").getAsJsonObject()
                                        .get(classEnum.name()).getAsJsonObject()
                                        .get("max").getAsString())
                        );
                    }
                    container.getChildren().add(hbox);
                }
                TitledPane tp = new TitledPane();
                tp.setText(rangeName);
                tp.setContent(container);
                accordion.getPanes().add(tp);
            }
        }
    }

    private VBox createNewEditBox(String name, String value){
        TextField textField = new TextField();
        textField.setText(value);
        return new VBox(new Label(name), textField);
    }

    private VBox createNewTimeBox(String name, JsonObject time) {
        return new VBox(new Label(name), new LocalTimeTextField(LocalTime.of(time.get("hour").getAsInt(), time.get("minute").getAsInt(), time.get("second").getAsInt(), time.get("nano").getAsInt())));
    }
}
