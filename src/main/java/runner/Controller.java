package runner;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import org.controlsfx.control.TaskProgressView;
import org.dom4j.rule.Rule;
import org.libtrader.api.exceptions.TWSException;
import org.libtrader.rules.FTGRange;
import org.libtrader.rules.TEntry;
import org.libtrader.rules.ThreeMLRange;
import org.libtrader.rules.celines.GenerateCELine;
import org.libtrader.types.Putup;
import org.libtrader.types.Util;
import org.libtrader.types.bars.BarPointGraph;
import org.libtrader.types.enums.PutupClass;
import org.libtrader.types.enums.PutupType;
import org.libtrader.types.ranges.TimeRange;
import org.libtrader.types.rules.DebugResult;
import org.libtrader.types.rules.RuleResult;
import org.libtrader.types.rules.generic.AbstractRule;
import tornadofx.control.DateTimePicker;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class Controller {
    private final boolean DEBUG = true;

    @FXML
    private ScrollPane threeMLLogScroll;

    @FXML
    private ScrollPane FTGLogScroll;

    @FXML
    private ScrollPane TEntryLogScroll;

    @FXML
    private TextFlow threeMLLogFlow;

    @FXML
    private TextFlow FTGLogFlow;

    @FXML
    private TextFlow TEntryLogFlow;

    @FXML
    private ComboBox<String> FTGClassCombo;

    @FXML
    private ComboBox<String> threeMLClassCombo;

    @FXML
    private TextField threeMLTickerInput;

    @FXML
    private TextField FTGTickerInput;

    @FXML
    private Alert errorBox;

    @FXML
    private CheckBox threeMLFBox;

    @FXML
    private CheckBox threeMLAllDayBox;

    @FXML
    private TextField threeMLPriceInput;

    @FXML
    private DateTimePicker threeMLDatePicker;

    @FXML
    private DatePicker FTGDatePicker;

    @FXML
    private TextField TEntryTickerInput;

    @FXML
    private TextField TEntryPriceInput;

    @FXML
    private ComboBox<String> TEntryClassCombo;

    @FXML
    private DateTimePicker TEntryDatePicker;

    @FXML
    private ComboBox<String> TEntryTradeCombo;

    @FXML
    private CheckBox TEntryAllDayBox;

    @FXML
    private Button TEntryRunButton;

    @FXML
    private Button CELineRunButton;

    @FXML
    private Button MagCompRunButton;

    @FXML
    private TaskProgressView<Task<? extends RuleResult>> mainTaskView;

    private SimpleIntegerProperty runningTasks;

    @FXML
    private TitledPane taskTitledPane;

    @FXML
    private void initialize() {
        runningTasks = new SimpleIntegerProperty(mainTaskView.getTasks().size());

//        taskTitledPane.setText(String.format("Show tasks (Running: %s)", mainTaskView.getTasks().size()));
//        taskTitledPane.sceneProperty().addListener((observableScene, oldScene, newScene) -> {
//            if (oldScene == null && newScene != null) {
//                taskTitledPane.heightProperty().addListener((observableHeight, oldHeight, newHeight) -> newScene.getWindow().sizeToScene());
//            }
//        });
//        Stage stage = (Stage) taskTitledPane.getScene().getWindow();

//        taskTitledPane.heightProperty().addListener((obs, oldHeight, newHeight) -> stage.sizeToScene());
        threeMLDatePicker.setFormat("dd/MM/yyyy HH:mm:ss");
        TEntryDatePicker.setFormat("dd/MM/yyyy HH:mm:ss");
        errorBox = new Alert(Alert.AlertType.ERROR);

//        mainTaskView.getTasks().addListener((ListChangeListener<Task<DebugResult>>) listener -> taskTitledPane.setText(String.format("Show tasks (Running: %s)", mainTaskView.getTasks().size())));
//        runningTasks.addListener(observable -> taskTitledPane.setText(String.format("Show tasks (Running: %s)", mainTaskView.getTasks().size())));

        threeMLLogFlow.getChildren().addListener(createScrollListener(threeMLLogFlow, threeMLLogScroll));

        FTGLogFlow.getChildren().addListener(createScrollListener(FTGLogFlow, FTGLogScroll));

        threeMLLogScroll.setContent(threeMLLogFlow);
        FTGLogScroll.setContent(FTGLogFlow);

        ObservableList<String> classes = FXCollections.observableArrayList("UU", "U", "PA", "PAP", "PP", "PH");
        ObservableList<String> tradeTypes = FXCollections.observableArrayList("Long", "Short");

        for (ComboBox<String> box : Arrays.asList(FTGClassCombo, threeMLClassCombo, TEntryClassCombo)) {
            box.setItems(classes);
            box.setValue(classes.get(0));
        }

        for (ComboBox<String> box : Arrays.asList(TEntryTradeCombo)) {
            box.setItems(tradeTypes);
            box.setValue(tradeTypes.get(0));
        }
        //Left in this implementation for extensibility, LCEntry and red exits will require this list.
    }

    @FXML
    private void on3MLRunButton() {

        Pattern tickerPattern = Pattern.compile("^[a-zA-Z]{1,5}$");
        Pattern pricePattern = Pattern.compile("^[0-9]+(\\.[0-9][0-9]?)?");

        if (threeMLAllDayBox.isSelected()) {
            threeMLDatePicker.setFormat("dd/MM/yyyy");
        }
        if (threeMLTickerInput.getText().isEmpty() || threeMLPriceInput.getText().isEmpty() || threeMLDatePicker.dateTimeValueProperty().isNull().get()) {
            missingFields();
        }else if (!tickerPattern.matcher(threeMLTickerInput.getText()).matches()){
            errorBox.setTitle("tickerPattern Mismatch");
            errorBox.setContentText("Invalid ticker");
            errorBox.show();
        }else if(!pricePattern.matcher(threeMLPriceInput.getText()).matches()){
            errorBox.setTitle("tickerPrice Mismatch");
            errorBox.setContentText("A price should be a number with two decimal places after it.");
            errorBox.show();
        }else if (!threeMLAllDayBox.isSelected() && (threeMLDatePicker.getDateTimeValue().toLocalTime().isBefore(Util.getExchOpeningLocalTime()) || threeMLDatePicker.getDateTimeValue().toLocalTime().isAfter(Util.getExchClosingLocalTime()))) {
            errorBox.setTitle("Time is out of range");
            errorBox.setContentText(String.format("Selected time is out of range of exchange hours (%s and %s)", Util.getExchOpeningLocalTime(), Util.getExchClosingLocalTime()));
            errorBox.show();
        } else {
            Putup putup = new Putup(threeMLTickerInput.getText(), PutupType.LONGS, Double.valueOf(threeMLPriceInput.getText()), PutupClass.valueOf(threeMLClassCombo.getValue()), threeMLFBox.isSelected());
            checkRule(putup, new ThreeMLRange<>(), threeMLLogFlow, false, threeMLAllDayBox.isSelected() ? threeMLDatePicker.getDateTimeValue().toLocalDate() : threeMLDatePicker.getDateTimeValue(), true, false);
        }
    }

    @FXML
    public void onFTGRunButton() {
        Pattern tickerPattern = Pattern.compile("^[a-zA-Z]{1,5}$");

        if (FTGTickerInput.getText().isEmpty() || FTGDatePicker.getValue().toString().isEmpty()) {
            missingFields();
        } else if (!tickerPattern.matcher(FTGTickerInput.getText()).matches()){
            errorBox.setTitle("tickerPattern Mismatch");
            errorBox.setContentText("Invalid ticker");
            errorBox.show();
        }else{
            Putup putup = new Putup(FTGTickerInput.getText(), PutupType.LONGS, 44.13, PutupClass.valueOf(FTGClassCombo.getValue()), false);
            checkRule(putup, new FTGRange<>(), FTGLogFlow, true, FTGDatePicker.getValue(), false, false);
        }

    }

    @FXML
    private void handleOnEditRanges(){
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("/EditRanges.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Edit Value Tables");
            stage.setScene(new Scene(root, 1024, 768));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void OnTEntryRunButton(){
        if (checkTEntryInformation()) {
            Putup putup = new Putup(TEntryTickerInput.getText(), PutupType.LONGS, Double.valueOf(TEntryPriceInput.getText()), PutupClass.valueOf(TEntryClassCombo.getValue()), threeMLFBox.isSelected());
            checkRule(putup, new TEntry<>(), TEntryLogFlow, true, TEntryDatePicker.getDateTimeValue(), false, true);
        }
    }

    @FXML
    public void OnCELineRunButton(){
        if (checkTEntryInformation()) {
            Putup putup = new Putup(TEntryTickerInput.getText(), PutupType.LONGS, Double.valueOf(TEntryPriceInput.getText()), PutupClass.valueOf(TEntryClassCombo.getValue()), threeMLFBox.isSelected());
            checkRule(putup, new GenerateCELine(), TEntryLogFlow, true, TEntryDatePicker.getDateTimeValue(), false, true);
        }
    }

    @FXML
    private void OnMagCompRunButton(){
        if (checkTEntryInformation()) {
            Putup putup = new Putup(TEntryTickerInput.getText(), PutupType.LONGS, Double.valueOf(TEntryPriceInput.getText()), PutupClass.valueOf(TEntryClassCombo.getValue()), threeMLFBox.isSelected());
//            checkRule(putup, new TEntry<>(), TEntryLogFlow, true, TEntryDatePicker.getDateTimeValue(), false);
        }
    }

    private boolean checkTEntryInformation() {
        Pattern tickerPattern = Pattern.compile("^[a-zA-Z]{1,5}$");
        Pattern pricePattern = Pattern.compile("^[0-9]+\\.[0-9]{2}?$");

        if (TEntryAllDayBox.isSelected()) {
            TEntryDatePicker.setFormat("dd/MM/yyyy");
        }
        if (TEntryTickerInput.getText().isEmpty() || TEntryPriceInput.getText().isEmpty() || TEntryDatePicker.dateTimeValueProperty().isNull().get()) {
            missingFields();
        }else if (!tickerPattern.matcher(TEntryTickerInput.getText()).matches()){
            errorBox.setTitle("tickerPattern Mismatch");
            errorBox.setContentText("Invalid ticker");
            errorBox.show();
        }else if(!pricePattern.matcher(TEntryPriceInput.getText()).matches()){
            errorBox.setTitle("tickerPrice Mismatch");
            errorBox.setContentText("A price should be a number with two decimal places after it.");
            errorBox.show();
        }else if (!TEntryAllDayBox.isSelected() && (TEntryDatePicker.getDateTimeValue().toLocalTime().isBefore(Util.getExchOpeningLocalTime()) || TEntryDatePicker.getDateTimeValue().toLocalTime().isAfter(Util.getExchClosingLocalTime()))) {
            errorBox.setTitle("Time is out of range");
            errorBox.setContentText(String.format("Selected time is out of range of exchange hours (%s and %s)", Util.getExchOpeningLocalTime(), Util.getExchClosingLocalTime()));
            errorBox.show();
        } else {
            return true;
        }
        return false;
    }

    private void missingFields() {
        errorBox.setTitle("Missing field(s)");
        errorBox.setContentText("Some fields are either missing or empty.");
        errorBox.show();
    }

    private ListChangeListener<Node> createScrollListener(TextFlow flow, ScrollPane scroll) {
        return c -> {
            flow.layout();
            scroll.layout();
            scroll.setVvalue(0.0f);
        };
    }

    private void checkRule(Putup putup, AbstractRule rule, TextFlow flow, boolean getPreviousLow, Temporal dateTime, boolean showPrice, boolean debug) {
        TimeRange range;
        if (dateTime instanceof LocalDate) {
            range = Util.getExchTimes();
        } else {
            range = new TimeRange(Util.getExchOpeningLocalTime(), ((LocalDateTime) dateTime).toLocalTime());
        }


        Task<? extends RuleResult> pointsTask;

        if (debug) {
            pointsTask = new Task<DebugResult>() {
                @Override
                public DebugResult call() throws Exception {
                    BarPointGraph points = Util.generateHistoricPoints(putup, LocalDate.from(dateTime), range, getPreviousLow);
                    points.addPreviousGraph(01012000, Util.generateHistoricPoints(putup, LocalDate.from(dateTime).minus(1, ChronoUnit.DAYS), new TimeRange(Util.getExchOpeningLocalTime(), Util.getExchClosingLocalTime()), false));


                    return rule.debug(points);
                }
            };
        }  else {
            pointsTask = new Task<RuleResult>() {
                @Override
                public RuleResult call() throws Exception {
                    return rule.run(Util.generateHistoricPoints(putup, LocalDate.from(dateTime), range, getPreviousLow));
                }
            };
        }
//        mainTaskView.getTasks().add(pointsTask);

        try {
            RuleResult r = rule.run(Util.generateHistoricPoints(putup, LocalDate.from(dateTime), range, getPreviousLow));
        } catch (TWSException e) {
            errorBox.setContentText(e.getLocalizedMessage());
            errorBox.show();
        }

//        Thread pointsThread = new Thread(pointsTask);
//        long startTime = System.currentTimeMillis();
//        pointsThread.start();
//        pointsTask.setOnSucceeded(event -> {
//            long endTime = System.currentTimeMillis();
//            mainTaskView.getTasks().remove(pointsTask);
//            RuleResult result = null;
//            try {
//                result = pointsTask.get();
//                Text resultText;
//                if (result.isPassed()) {
//                    resultText = new Text("Passed");
//                    resultText.setFill(Color.GREEN);
//                } else {
//                    resultText = new Text("Failed");
//                    resultText.setFill(Color.RED);
//                }
//
//                String dateTimeString = DEBUG ? String.format("%s. (Took %s)", dateTime, String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(endTime - startTime), TimeUnit.MILLISECONDS.toSeconds(endTime - startTime))) : dateTime.toString();
//                Text outputText = new Text(String.format(" for %s of class %s%sat date/time %s.\n", putup.getTickerCode(), putup.getClass(), showPrice ? String.format(" with price(s) %s ", putup.getPriceList()) : " ", dateTimeString));
//                flow.getChildren().add(new Text(String.format("[ %s ] ", DateTimeFormatter.ofPattern("dd/MM/yy HH:mm:ss").format(LocalDateTime.now()))));
//                flow.getChildren().add(resultText);
//                flow.getChildren().add(outputText);
//
//                if (debug) {
//                    for (Object dbg: ((DebugResult) result).getDebugInfo())
//                        flow.getChildren().add(new Text(dbg.toString()+'\n'));
//                    flow.getChildren().add(new Text("===================================================\n"));
//                }
//
//            } catch (InterruptedException | ExecutionException e) {
//                e.printStackTrace();
//            }
//        });
    }

    public void on3MLPassingRunButton(ActionEvent actionEvent) {
        Putup putup = new Putup("JJG", PutupType.LONGS, 28.64, PutupClass.UU, false);
        checkRule(putup, new ThreeMLRange<>(), threeMLLogFlow, false, LocalDate.of(2016, 7, 29), true, false);
    }
}
